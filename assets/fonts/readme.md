# MonkeyTheme

### Directory path
framework/assets/fonts

### File format
Font format:
* TTF/OTF
* WOFF
* WOFF2 (Not supported in all Browsers)
* SVG (Not supported in all Browsers)
* EOT (Not supported in all Browsers)

### Information
Try to use multiple font formats to ensure that it works in all new browsers

[Browser Support](http://www.w3schools.com/cssref/css3_pr_font-face_rule.asp)

