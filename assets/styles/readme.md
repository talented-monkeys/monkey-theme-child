# MonkeyTheme

### Directory path
framework/assets/sass

### File format
Only "sass" files allowed
example: page.sass

### Information
We use multiple subfolders to have a good overview of the project.

* "basis" folder for example, contains things like headlines, lists and so on. All tags that are globally use over the site.
* "pages" contains sass files that are only use for a type of page

We could use more folders on a later stage, like "plugins" for framework plugins
