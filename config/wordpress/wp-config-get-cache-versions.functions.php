<?php

	/*
	** Read the different versions from cache control gulp task
	*/

	$cache_versions_path = realpath(dirname(__FILE__) . '/..') . '/cache/version';

	// Get timestamp version
	$cache_versions_timestamp_file = $cache_versions_path.'/.timestamp-version';
	$cache_versions_timestamp_version = fgets(fopen($cache_versions_timestamp_file, 'r'));
	define('MT_CHILD_CACHE_VERSION_TIMESTAMP', $cache_versions_timestamp_version);

	// Get date version
	$cache_versions_date_file = $cache_versions_path.'/.date-version';
	$cache_versions_date_version = fgets(fopen($cache_versions_date_file, 'r'));
	define('MT_CHILD_CACHE_VERSION_DATE', $cache_versions_date_version);

	// Get project version
	$cache_versions_project_file = $cache_versions_path.'/.project-version';
	$cache_versions_project_version = fgets(fopen($cache_versions_project_file, 'r'));
	define('MT_CHILD_CACHE_VERSION_PROJECT', $cache_versions_project_version);

	// Get git-hash version
	$cache_versions_hash_file = $cache_versions_path.'/.hash-version';
	$cache_versions_hash_version = fgets(fopen($cache_versions_hash_file, 'r'));
	define('MT_CHILD_CACHE_VERSION_HASH', $cache_versions_hash_version);

	// Get git-hash (short) version
	$cache_versions_hash_short_file = $cache_versions_path.'/.hash-short-version';
	$cache_versions_hash_short_version = fgets(fopen($cache_versions_hash_short_file, 'r'));
	define('MT_CHILD_CACHE_VERSION_HASH_SHORT', $cache_versions_hash_short_version);

