<?php

	/*
	* Individual Child Theme functions
	*
	*

	Example:

	function my_dequeue() {
		// Removing atf scripts and styles
		wp_deregister_script( 'parent-atf-scripts' );
		wp_dequeue_script( 'parent-atf-scripts' );
		wp_deregister_script( 'child-atf-scripts' );
		wp_dequeue_script( 'child-atf-scripts' );

		wp_deregister_style( 'parent-atf-style' );
		wp_dequeue_style( 'parent-atf-style' );
		wp_deregister_style( 'child-atf-style' );
		wp_dequeue_style( 'child-atf-style' );
	}

	add_action( 'wp_footer', 'my_dequeue', 10 );

	*
	*/
