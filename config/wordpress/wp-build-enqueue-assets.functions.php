<?php

	/*
	* Enqeueue assets
	*/

	function theme_child_styles() {
		$child_theme_css_uri = get_stylesheet_directory_uri() . '/dist/css/combined';
		$child_theme_css_path = get_stylesheet_directory() . '/dist/css/combined';
		$child_theme_css_cache_version = 'c_'.MT_CHILD_CACHE_VERSION_TIMESTAMP;

		$assetCssFiles = array('preloader', 'atf', 'project', 'modules', 'vendor');

		foreach ($assetCssFiles as $assetCssFile) {
			if(file_exists ( $child_theme_css_path.'/'.$assetCssFile.'.min.css' )){
				wp_register_style('child-'.$assetCssFile.'-style',	$child_theme_css_uri.'/'.$assetCssFile.'.min.css',	array('parent-'.$assetCssFile.'-style'), $child_theme_css_cache_version, 'screen');
				wp_enqueue_style('child-'.$assetCssFile.'-style');
			}
		}
	}

	function theme_child_scripts() {
		$child_theme_js_uri = get_stylesheet_directory_uri() . '/dist/js/combined';
		$child_theme_js_path = get_stylesheet_directory() . '/dist/js/combined';
		$child_theme_js_cache_version = 'c_'.MT_CHILD_CACHE_VERSION_HASH_SHORT;

		$assetJsFiles = array('preloader', 'atf', 'project', 'modules', 'vendor');

		foreach ($assetJsFiles as $assetJsFile) {
			if(file_exists ( $child_theme_js_path.'/'.$assetJsFile.'.min.js' )){
				wp_register_script('child-'.$assetJsFile.'-scripts', $child_theme_js_uri.'/'.$assetJsFile.'.min.js', array('parent-'.$assetJsFile.'-scripts'), $child_theme_js_cache_version, true);
				wp_enqueue_script('child-'.$assetJsFile.'-scripts');
			}
		}
	}

	add_action('wp_enqueue_styles', 'theme_child_styles', 1);
	add_action('wp_enqueue_scripts', 'theme_child_scripts', 1);
