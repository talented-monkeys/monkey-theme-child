<?php

	/*
	* Child Theme functions
	*/

	// Getting versions for cache controle from the gulp tasks
	include get_stylesheet_directory() . '/config/wordpress/wp-config-get-cache-versions.functions.php';
	// Usage of the cache control const:
	// - MT_CHILD_CACHE_VERSION_TIMESTAMP 	=> Timestamp of the the last assets build gulp task
	// - MT_CHILD_CACHE_VERSION_DATE 				=> Date (format yyyymmdd) of the the last assets build gulp task
	// - MT_CHILD_CACHE_VERSION_PROJECT 		=> Version number of the package.json
	// - MT_CHILD_CACHE_VERSION_HASH 				=> Current git commit hash
	// - MT_CHILD_CACHE_VERSION_HASH_SHORT 	=> Current git commit hash (short version)

	// Include wordpress child theme assets enqueue scripts
	include get_stylesheet_directory() . '/config/wordpress/wp-build-enqueue-assets.functions.php';

	// Include custom functions
	include get_stylesheet_directory() . '/config/wordpress/inc.functions.php';
